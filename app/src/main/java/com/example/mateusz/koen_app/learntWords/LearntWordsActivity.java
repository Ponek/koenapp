package com.example.mateusz.koen_app.learntWords;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.example.mateusz.koen_app.R;
import com.example.mateusz.koen_app.Word;
import com.example.mateusz.koen_app.base.BaseActivity;
import com.example.mateusz.koen_app.finals.Finals;
import com.example.mateusz.koen_app.listAdapters.WordAdapter;
import com.example.mateusz.koen_app.testActivity.TestActivity;
import com.example.mateusz.koen_app.testLearntWordsActivity.TestLearntWordsAcitvity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class LearntWordsActivity extends BaseActivity implements LearntWordsContract.View {

    private LearntWordsContract.Presenter presenter;
    LearntWordsViewHolder viewHolder;
    private int allWords = 0;
    private String wordsType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView();
        Intent intent = getIntent();
        wordsType = intent.getStringExtra("type");

        setPresenter();
        setSpinnerChapters();
        setWordsLimit();

    }

    public void setView() {
        setContentView(R.layout.activity_learnt_words);
        viewHolder = new LearntWordsViewHolder(findViewById(android.R.id.content));
        viewHolder.start_test.setOnClickListener(v -> onStartTestClick());

    }

    @Override
    public void onStartTestClick() {
        Intent intent = new Intent(this, TestLearntWordsAcitvity.class);
        startActivity(intent);
    }

    public void setPresenter() {
        presenter = new LearntWordsPresenter();
        ((LearntWordsPresenter) presenter).onLoad(this);
    }


    @Override
    public void setSpinnerChapters() {
        Finals.myRefChapter.child(Finals.AUTH_INSTANCE.getUid()).child("learned").child(wordsType).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> chaptersList = new ArrayList<>();
                if (dataSnapshot.exists()) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        Log.d("chapters", data.getKey());
                        chaptersList.add(data.getKey());
                    }


                    ArrayAdapter<String> chaptersAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, chaptersList);
                    viewHolder.select_chapter.setAdapter(chaptersAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void setWordsLimit() {

        viewHolder.select_range.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                tempRef = Finals.myRefAddWordsSet.push();

                String rozmiar = viewHolder.select_range.getSelectedItem().toString().replace(" words", " ").trim();
                if (rozmiar.equals("All")) {
                    setWordsList(viewHolder.select_chapter.getSelectedItem().toString(), allWords);
                    System.out.println("Rozmiar all: " + allWords);
                } else {
                    setWordsList(viewHolder.select_chapter.getSelectedItem().toString(), Integer.valueOf(viewHolder.select_range.getSelectedItem().toString().replace(" words", " ").trim()));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
        viewHolder.select_chapter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                tempRef = Finals.myRefAddWordsSet.push();

                String rozmiar = viewHolder.select_range.getSelectedItem().toString().replace(" words", " ").trim();
                if (rozmiar.equals("All")) {
                    setWordsList(viewHolder.select_chapter.getSelectedItem().toString(), allWords);
                    System.out.println("Rozmiar all: " + allWords);
                } else {
                    setWordsList(viewHolder.select_chapter.getSelectedItem().toString(), Integer.valueOf(viewHolder.select_range.getSelectedItem().toString().replace(" words", " ").trim()));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void setWordsList(String chapter, int range) {
        Finals.database.getReference("users").child(Finals.AUTH_INSTANCE.getCurrentUser().getUid()).child("repeatingSet").removeValue();
        Finals.myRefChapter.child(Finals.AUTH_INSTANCE.getUid()).child("learned").child(wordsType).child(chapter).addListenerForSingleValueEvent(new ValueEventListener() {
            List<Word> list = new ArrayList<>();
            List<Word> adapterList = new ArrayList<>();
            List<Integer> randomWordsNumbers;

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot iter : dataSnapshot.getChildren()) {
                        Word word = new Word(iter.getKey(), iter.getValue().toString());
                        list.add(word);
                        allWords++;
                    }

                    if(list.size() < range){
                        randomWordsNumbers = presenter.randomWordsNumber(list.size());
                        for (int i = 0; i < list.size(); i++) {
                            adapterList.add(new Word(list.get(i).getInEnglish(), list.get(i).getInKorean()));
                            Finals.database.getReference("users").child(Finals.AUTH_INSTANCE.getCurrentUser().getUid()).child("repeatingSet").child(chapter).child(adapterList.get(i).getInEnglish()).setValue(adapterList.get(i).getInKorean());
                        }
                    }
                    else{
                        randomWordsNumbers = presenter.randomWordsNumber(range);
                        for (int i = 0; i < range; i++) {
                            adapterList.add(new Word(list.get(i).getInEnglish(), list.get(i).getInKorean()));
                            Finals.database.getReference("users").child(Finals.AUTH_INSTANCE.getCurrentUser().getUid()).child("repeatingSet").child(chapter).child(adapterList.get(i).getInEnglish()).setValue(adapterList.get(i).getInKorean());
                        }
                    }



                    WordAdapter wordAdapter = new WordAdapter(adapterList);
                    viewHolder.word_list.setAdapter(wordAdapter);
                    viewHolder.word_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public LearntWordsViewHolder getLearntWordsViewHolder() {
        return viewHolder;
    }

    @Override
    public LearntWordsContract.Presenter getPresenter() {
        return presenter;
    }
}
