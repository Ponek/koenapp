package com.example.mateusz.koen_app.mainActivity;

import android.content.Context;

interface MainContract {
    interface View{
        MainContract.MainPresenter getPresenter();
        MainViewHolder getMainViewHolder();
        void goToStartLearningActivity();
        void gotoEditWordActivity();
        void openDialog();

    }
    interface MainPresenter{
        void onLoad(View  mainView);
        void addNewWord(Context context,String wordInEnglish, String wordInKorean,String chapter);
    }
}
