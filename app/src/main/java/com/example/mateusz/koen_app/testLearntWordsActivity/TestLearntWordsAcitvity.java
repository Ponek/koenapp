package com.example.mateusz.koen_app.testLearntWordsActivity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.mateusz.koen_app.R;
import com.example.mateusz.koen_app.Word;
import com.example.mateusz.koen_app.base.BaseActivity;
import com.example.mateusz.koen_app.finals.Finals;
import com.example.mateusz.koen_app.mainActivity.MainActivity;
import com.example.mateusz.koen_app.testActivity.TestActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TestLearntWordsAcitvity extends BaseActivity implements TestLearntWordsContract.View {

    private TestLearntWordsContract.Presenter presenter;
    private TestLearntWordsViewHolder viewHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView();
        setPresenter();
        setFields();
    }

    public void setView() {
        setContentView(R.layout.activity_test_learnt_words_acitvity);
        viewHolder = new TestLearntWordsViewHolder(findViewById(android.R.id.content));
        viewHolder.test_check.setOnClickListener(v -> presenter.checkWords(this,viewHolder.test_in_korean.getText().toString(), viewHolder.test_in_english.getText().toString()));

    }


    @Override
    public void setFields() {
        Finals.database.getReference("users").child(Finals.AUTH_INSTANCE.getUid()).child("repeatingSet").addListenerForSingleValueEvent(new ValueEventListener() {
            ArrayList<Word> list = new ArrayList<>();

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        Log.d("chapter", data.getKey());
                        for (DataSnapshot wordSnapshot : data.getChildren()) {
                            Log.d("english", wordSnapshot.getKey());
                            Log.d("korean", wordSnapshot.getValue().toString());

                            list.add(new Word(wordSnapshot.getKey(), wordSnapshot.getValue().toString()));
                        }
                    }
                    viewHolder.test_in_english.setText(list.get(presenter.randomNUmber(list.size())).getInEnglish());
                    System.out.println("Slowko wylosowane: " + viewHolder.test_in_english.getText());
                } else {
                    Toast.makeText(TestLearntWordsAcitvity.this, "Good job!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(TestLearntWordsAcitvity.this, MainActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }


    public void setPresenter() {
        presenter = new TestLearntWordsPresenter();
        ((TestLearntWordsPresenter) presenter).onLoad(this);
    }

    @Override
    public TestLearntWordsViewHolder getTestLearntWordsViewHolder() {
        return viewHolder;
    }

    @Override
    public TestLearntWordsContract.Presenter getPresenter() {
        return presenter;
    }
}
