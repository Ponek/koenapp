package com.example.mateusz.koen_app.testActivity;

import android.content.Context;

interface TestActivityContract  {
    interface View{
        TestActivityContract.Presenter getPresenter();
        TestActivityViewHolder getTestActivityViewHolder();
    void setFields();
    }
    interface Presenter{
        int randomNUmber(int range);
        boolean checkWords(Context context, String koreanInput, String englishWord,String wordsType);

    }
}
