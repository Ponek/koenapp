package com.example.mateusz.koen_app.loginAndRegisterActivity;

interface LoginAndRegisterContract {
    interface View{
        LoginAndRegisterContract.Presenter getPresenter();
        LoginAndRegisterViewHolder getLoginAndRegisterViewHolder();
        boolean validateFields();
    }
    interface Presenter{
        boolean registrationEmailAndPassword(String email,String password);
        boolean loginEmailAndPassword(String email,String password);
        void sendEmailVerification();
        void createUserWordsDatabase();
    }
}
