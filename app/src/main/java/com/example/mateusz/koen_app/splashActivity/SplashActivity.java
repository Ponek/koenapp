package com.example.mateusz.koen_app.splashActivity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.example.mateusz.koen_app.R;
import com.example.mateusz.koen_app.base.BaseActivity;
import com.example.mateusz.koen_app.finals.Finals;
import com.example.mateusz.koen_app.loginAndRegisterActivity.LoginAndRegisterActivity;
import com.example.mateusz.koen_app.mainActivity.MainActivity;
import com.example.mateusz.koen_app.sharedPreferences.SaveUserState;

public class SplashActivity extends BaseActivity implements SplashActivityContract.View {

    SaveUserState savedUserState = new SaveUserState();
    private SplashActivityContract.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView();
        ifUserExistsStartNewIntent();
    }

    private void setView() {
        setContentView(R.layout.activity_splash);
        setPresenter();
        createNotificationChannel();
    }

    public void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "name";
            String description = "desc";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("112", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


    private void setPresenter() {
        presenter = new SplashActivityPresenter();
        ((SplashActivityPresenter) presenter).onLoad(this);
    }

    @Override
    public void ifUserExistsStartNewIntent() {
        if (presenter.checkUserState()) {
            Finals.AUTH_INSTANCE.signInWithEmailAndPassword(savedUserState.getUserEmail(this), savedUserState.getUserPassword(this));
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, LoginAndRegisterActivity.class);
            startActivity(intent);
        }
    }
}
