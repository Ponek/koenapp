package com.example.mateusz.koen_app.editWordActivity;

import java.util.List;

public interface EditWordContract {
    interface View{
        EditWordViewHolder getEditWordViewHolder();
        EditWordContract.Presenter getPresenter();
        void setWordsLimits();
        void setWordsList(String chapter,int range);
        void searchWord();
        void setSpinnerChapters();
       }
    interface Presenter{
        List<Integer> randomWordsNumber(int size);
        void updateWord(String wordInEnglish,String wordInKorean,String chapter,String wordsType);
        void updateWordAndRemoveOldOne(String wordInEnglish,String wordInKorean,String chapter,String oldChapter,String wordsType);
    }
}
