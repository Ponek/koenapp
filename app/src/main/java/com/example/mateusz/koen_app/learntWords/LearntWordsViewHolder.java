package com.example.mateusz.koen_app.learntWords;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

import com.example.mateusz.koen_app.R;

public class LearntWordsViewHolder {

    RecyclerView word_list;
    Spinner select_chapter;
    Spinner select_range;
    Button start_test;

    public LearntWordsViewHolder(View root) {
        this.word_list = root.findViewById(R.id.words_list);
        this.select_chapter = root.findViewById(R.id.select_chapter);
        this.select_range = root.findViewById(R.id.select_range);
        this.start_test = root.findViewById(R.id.start_learning);

    }

}
