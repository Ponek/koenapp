package com.example.mateusz.koen_app.splashActivity;

interface SplashActivityContract {
    interface View {
        void ifUserExistsStartNewIntent();
    }

    interface Presenter {
        boolean checkUserState();
    }
}
