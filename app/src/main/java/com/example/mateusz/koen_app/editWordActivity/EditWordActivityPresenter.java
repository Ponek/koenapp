package com.example.mateusz.koen_app.editWordActivity;

import com.example.mateusz.koen_app.base.BasePresenter;
import com.example.mateusz.koen_app.finals.Finals;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class EditWordActivityPresenter extends BasePresenter<EditWordContract.View> implements EditWordContract.Presenter {
    @Override
    public List<Integer> randomWordsNumber(int size) {
        List<Integer> list = new ArrayList<Integer>();
        Random rand = new Random();

        do {
            int randomInt = rand.nextInt(size + 1);

            if(!list.contains(randomInt)){
                list.add(randomInt);
            }

        } while (list.size() != size);

        return list;

    }

    @Override
    public void updateWord(String wordInEnglish, String wordInKorean, String chapter, String wordsType) {
        Finals.myRefChapter.child(Finals.AUTH_INSTANCE.getUid()).child("Words").child(wordsType).child(chapter).child(wordInEnglish).setValue(wordInKorean);

    }

    @Override
    public void updateWordAndRemoveOldOne(String wordInEnglish, String wordInKorean, String chapter,String oldChapter,String wordsType) {
        Finals.myRefChapter.child(Finals.AUTH_INSTANCE.getUid()).child("Words").child(wordsType).child(oldChapter).child(wordInEnglish).removeValue();
        Finals.myRefChapter.child(Finals.AUTH_INSTANCE.getUid()).child("Words").child(wordsType).child(chapter).child(wordInEnglish).setValue(wordInKorean);
    }
}
