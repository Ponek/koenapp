package com.example.mateusz.koen_app.mainActivity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.example.mateusz.koen_app.R;
import com.example.mateusz.koen_app.Word;
import com.example.mateusz.koen_app.base.BasePresenter;
import com.example.mateusz.koen_app.finals.Finals;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainPresenter extends BasePresenter<MainContract.View> implements MainContract.MainPresenter {

    @Override
    public void addNewWord(Context context, String wordInEnglish, String wordInKorean, String chapter) {
        ArrayList<Word> list = new ArrayList<>();

        Finals.myRefChapter.child(Finals.AUTH_INSTANCE.getUid()).child("Words").child("Vocabulary").child(chapter).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        list.add(new Word(data.getKey(), data.getValue().toString()));
                    }
                    boolean exists = false;
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getInEnglish().equals(wordInEnglish)) {
                            exists = true;
                        }
                    }
                    if (exists) {
                        Toast.makeText(context, R.string.already_exist, Toast.LENGTH_SHORT).show();
                    } else {
                        Finals.myRefChapter.child(Finals.AUTH_INSTANCE.getUid()).child("Words").child("Vocabulary").child(chapter).child(wordInEnglish).setValue(wordInKorean);


                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
