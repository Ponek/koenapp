package com.example.mateusz.koen_app.editWordActivity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Spinner;

import com.example.mateusz.koen_app.R;

class EditWordViewHolder {
    RecyclerView word_list;
    Spinner select_chapter;
    Spinner select_range;
    Button start_test;
    SearchView search_word;

    EditWordViewHolder(View root) {
        this.word_list = root.findViewById(R.id.words_list);
        this.select_chapter = root.findViewById(R.id.select_chapter);
        this.select_range = root.findViewById(R.id.select_range);
        this.start_test = root.findViewById(R.id.start_learning);
        this.search_word= root.findViewById(R.id.search_word);
    }
}
