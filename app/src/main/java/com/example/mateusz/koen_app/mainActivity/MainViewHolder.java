package com.example.mateusz.koen_app.mainActivity;

import android.view.View;
import android.widget.Button;

import com.example.mateusz.koen_app.R;

public class MainViewHolder {
    Button start_learning;
    Button add_word;
    Button edit_word;
    Button learnt_words;


    public MainViewHolder(View root) {
        this.start_learning = root.findViewById(R.id.start_learning);
        this.add_word = root.findViewById(R.id.add_word);
        this.edit_word = root.findViewById(R.id.edit_word);
        this.learnt_words = root.findViewById(R.id.learnt_words);
    }
}
