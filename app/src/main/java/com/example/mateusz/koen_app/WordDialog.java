package com.example.mateusz.koen_app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.mateusz.koen_app.editWordActivity.EditWordActivityPresenter;
import com.example.mateusz.koen_app.finals.Finals;
import com.example.mateusz.koen_app.learningActivity.StartLearningActivity;
import com.example.mateusz.koen_app.mainActivity.MainPresenter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class WordDialog {
    private EditText editTextEnglishWord;
    private EditText editTextKoreanWord;
    private Spinner selectChapter;
    private ImageButton addChapter;
    private EditWordActivityPresenter editWordActivityPresenter = new EditWordActivityPresenter();
    private MainPresenter mainPresenter = new MainPresenter();


    public void createWordsTypeDialog(Activity activity,Class destinationClass) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_dialog_choose_words_type, null);


        Spinner spinner = view.findViewById(R.id.select_words_type);


        ArrayList<String> list = new ArrayList<>();
        list.add("Difficult Words");
        list.add("Vocabulary");
        ArrayAdapter<String> chaptersAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_dropdown_item, list);
        spinner.setAdapter(chaptersAdapter);

        builder.setView(view)
                .setTitle("Select words type")
                .setPositiveButton("ok", (dialog, which) -> {
            Log.d("selected type",spinner.getSelectedItem().toString());

                    Intent intent = new Intent(activity, destinationClass);
                    intent.putExtra("type",spinner.getSelectedItem().toString());
                    activity.startActivity(intent);
                });

        builder.create();
        builder.show();
    }

    public void createEmptyDialog(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_dialog_add, null);

        editTextEnglishWord = view.findViewById(R.id.edit_englishWord);
        editTextKoreanWord = view.findViewById(R.id.edit_koreanWord);
        selectChapter = view.findViewById(R.id.select_chapter);
        addChapter = view.findViewById(R.id.addChapter);

        setSpinnerChapters(selectChapter, activity.getApplicationContext());

        addChapter.setOnClickListener(v -> {
            MaterialDialog.Builder materialDialog = new MaterialDialog.Builder(activity);
            materialDialog.title("Type new chapter name");
            materialDialog.input("chapter name", null, false, new MaterialDialog.InputCallback() {
                @Override
                public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                    Finals.database.getReference().child("users").child(Finals.AUTH_INSTANCE.getUid()).child("Words").child("Vocabulary").child(input.toString()).setValue("null");
                }
            });
            materialDialog.show();
        });

        builder.setView(view)
                .setTitle("Add Word")
                .setNegativeButton("cancel", (dialog, which) -> {

                })
                .setPositiveButton("ok", (dialog, which) -> {
                    String englishWord = editTextEnglishWord.getText().toString();
                    String koreanword = editTextKoreanWord.getText().toString();
                    String chapter = selectChapter.getSelectedItem().toString();
                    mainPresenter.addNewWord(activity, englishWord, koreanword, chapter);
                });

        builder.create();
        builder.show();
    }


    private void setSpinnerChapters(Spinner spinnerChapters, Context context) {
        Finals.myRefChapter.child(Finals.AUTH_INSTANCE.getUid()).child("Words").child("Vocabulary").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> chaptersList = new ArrayList<>();
                if (dataSnapshot.exists()) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        Log.d("chapters", data.getKey());
                        chaptersList.add(data.getKey());
                    }


                    ArrayAdapter<String> chaptersAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, chaptersList);
                    spinnerChapters.setAdapter(chaptersAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void createDialogToEditWord(Activity activity, String wordInEnglish, String
            wordInKorean, String oldChapter,String wordsType) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_dialog_edit, null);

        editTextEnglishWord = view.findViewById(R.id.edit_englishWord);
        editTextKoreanWord = view.findViewById(R.id.edit_koreanWord);
        selectChapter = view.findViewById(R.id.select_chapter);

        editTextEnglishWord.setText(wordInEnglish);
        editTextKoreanWord.setText(wordInKorean);


        builder.setView(view)
                .setTitle("Add Word")
                .setNegativeButton("cancel", (dialog, which) -> {

                })
                .setPositiveButton("ok", (dialog, which) -> {
                    String englishWord = editTextEnglishWord.getText().toString();
                    String koreanword = editTextKoreanWord.getText().toString();
//                    String chapter = selectChapter.getSelectedItem().toString();

//                    if (!selectChapter.getSelectedItem().toString().equals(oldChapter)) {
//                        editWordActivityPresenter.updateWordAndRemoveOldOne(englishWord, koreanword,oldChapter, oldChapter,wordsType);
//
//                        TODO poprawić odswieżanie listy po zmianie chaptera
//                        activity.recreate();
//
//                    } else {
                        editWordActivityPresenter.updateWord(englishWord, koreanword, oldChapter,wordsType);
                        activity.recreate();

//                    }
                });

        builder.create();
        builder.show();
    }

}
