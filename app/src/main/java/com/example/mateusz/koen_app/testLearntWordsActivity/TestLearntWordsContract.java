package com.example.mateusz.koen_app.testLearntWordsActivity;

import android.content.Context;

interface TestLearntWordsContract {
    interface View {
        TestLearntWordsViewHolder getTestLearntWordsViewHolder();

        TestLearntWordsContract.Presenter getPresenter();

        void setFields();
    }

    interface Presenter {
        int randomNUmber(int range);

        boolean checkWords(Context context, String koreanInput, String englishWord);

    }

}
