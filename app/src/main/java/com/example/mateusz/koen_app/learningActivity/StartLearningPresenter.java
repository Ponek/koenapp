package com.example.mateusz.koen_app.learningActivity;

import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.example.mateusz.koen_app.base.BasePresenter;
import com.example.mateusz.koen_app.finals.Finals;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StartLearningPresenter extends BasePresenter<StartLearningContract.View> implements StartLearningContract.Presenter {
    @Override
    public List<Integer> randomWordsNumber(int size) {
        List<Integer> list = new ArrayList<Integer>();
        Random rand = new Random();

        do {
            int randomInt = rand.nextInt(size + 1);

            if(!list.contains(randomInt)){
                list.add(randomInt);
            }

        } while (list.size() != size);

        return list;

    }

}
