package com.example.mateusz.koen_app.sharedPreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class SaveUserState {
    private String email = "email";
    private String password = "password";

    private SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }


    public void setUserState(Context context, String userEmail, String userPassword) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(email, userEmail);
        editor.putString(password, userPassword);
        editor.apply();
        Log.d("User state","is saved");
    }

    public String getUserEmail(Context context) {
        return getSharedPreferences(context).getString(email, "");
    }

    public String getUserPassword(Context context) {
        return getSharedPreferences(context).getString(password, "");
    }
}
