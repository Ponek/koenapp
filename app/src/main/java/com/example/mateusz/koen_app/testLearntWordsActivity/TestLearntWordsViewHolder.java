package com.example.mateusz.koen_app.testLearntWordsActivity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.mateusz.koen_app.R;

public class TestLearntWordsViewHolder {
    TextView test_in_english;
    EditText test_in_korean;
    Button test_check;

    public TestLearntWordsViewHolder(View root) {
        test_check = root.findViewById(R.id.test_check);
        test_in_english = root.findViewById(R.id.test_in_english);
        test_in_korean = root.findViewById(R.id.test_in_korean);
    }
}
