package com.example.mateusz.koen_app.base;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity implements BaseContract.ViewCompat{
    @Override
    public Activity getActivity() {
        return this;
    }
}

