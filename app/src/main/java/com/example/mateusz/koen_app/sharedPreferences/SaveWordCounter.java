package com.example.mateusz.koen_app.sharedPreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.Map;

public class SaveWordCounter {
    private String word;
    private int counter;

    private SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }


    public void setCounterState(Context context, String wordInEnglish,String wordInKorean, int counter) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(wordInEnglish+ "/" + wordInKorean , String.valueOf(counter));
        editor.apply();
        Log.d("Counter state", "is saved");
    }

    public int getWordCounter(Context context, String wordInEnglish,String wordInKorean) {

        String sharedCounter = getSharedPreferences(context).getString(wordInEnglish+ "/" + wordInKorean , "");
        if (!sharedCounter.isEmpty()) {
            return Integer.parseInt(sharedCounter);
        } else {
            return 0;
        }
    }
    public void countDifficultWords(Context context){

        SharedPreferences mySPrefs =PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = mySPrefs.edit();

        Map<String, ?> allEntries = getSharedPreferences(context).getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
           if(!entry.getKey().equals("password") && !entry.getKey().equals("email")){
               if(Integer.valueOf(entry.getValue().toString()) ==3){
                   Log.d("BIGGER", entry.getKey());


                   //słówko po angielsku
                   String englishWord= entry.getKey().split("/")[0];

                   //słówko po koreańsku
                   String koreanhWord= entry.getKey().split("/")[1];


                   Log.d("KOREAN TEST",koreanhWord);
                   Log.d("ENGLISH TEST",englishWord);

                   //TODO tutaj wyślij to słówko gdzie tma chcesz




                   editor.remove(entry.getKey());
                   editor.apply();



               };
           };
        }
    }

}