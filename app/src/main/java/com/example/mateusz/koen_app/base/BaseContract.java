package com.example.mateusz.koen_app.base;

import android.app.Activity;

interface BaseContract {
    interface ViewCompat extends AndroidView,View{}
    interface View{

    }
    interface AndroidView{
        Activity getActivity();
    }
}
