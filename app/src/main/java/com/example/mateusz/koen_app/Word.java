package com.example.mateusz.koen_app;

public class Word {
    String inEnglish;
    String inKorean;

    public Word() {
    }

    public Word(String inEnglish, String inKorean) {
        this.inEnglish = inEnglish;
        this.inKorean = inKorean;
    }


    public String getInEnglish() {
        return inEnglish;
    }

    public void setInEnglish(String inEnglish) {
        this.inEnglish = inEnglish;
    }

    public String getInKorean() {
        return inKorean;
    }

    public void setInKorean(String inKorean) {
        this.inKorean = inKorean;
    }

    @Override
    public String toString() {
        return "In English '" + inEnglish + '\'' +
                ", In Korean='" + inKorean + '\'';
    }
}
