package com.example.mateusz.koen_app.loginAndRegisterActivity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.mateusz.koen_app.R;

public class LoginAndRegisterViewHolder {

    EditText email_field;
    EditText password_field;
    Button login_in_button;
    Button register_button;
    TextView email_hint;
    TextView password_hint;


    LoginAndRegisterViewHolder(View root) {
        this.email_field = root.findViewById(R.id.email_field);
        this.password_field = root.findViewById(R.id.password_field);
        this.login_in_button = root.findViewById(R.id.login_button);
        this.register_button = root.findViewById(R.id.register_button);
        this.email_hint = root.findViewById(R.id.email_hint);
        this.password_hint = root.findViewById(R.id.password_hint);
    }
}
