package com.example.mateusz.koen_app.testLearntWordsActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.mateusz.koen_app.R;
import com.example.mateusz.koen_app.base.BasePresenter;
import com.example.mateusz.koen_app.finals.Finals;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.Random;

public class TestLearntWordsPresenter extends BasePresenter<TestLearntWordsContract.View> implements TestLearntWordsContract.Presenter {

    @Override
    public int randomNUmber(int range) {
        Random rand = new Random();
        return rand.nextInt(range);

    }

    @Override
    public boolean checkWords(Context context, String koreanInput, String englishWord) {
        Finals.database.getReference("users").child(Finals.AUTH_INSTANCE.getUid()).child("repeatingSet").addListenerForSingleValueEvent(new ValueEventListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    for(DataSnapshot data : dataSnapshot.getChildren()){
                        Log.d("chapter", data.getKey());
                        for(DataSnapshot wordSnapshot : data.getChildren()){
                            Log.d("english", wordSnapshot.getKey());
                            Log.d("korean", wordSnapshot.getValue().toString());

                            if(koreanInput.equals(wordSnapshot.getValue().toString())) {

                                getView().setFields();
                                getView().getTestLearntWordsViewHolder().test_in_korean.setText("");

                                Toast.makeText(context, "Correct", Toast.LENGTH_SHORT).show();

                                Finals.database.getReference("users").child(Finals.AUTH_INSTANCE.getUid()).child("repeatingSet").child(data.getKey()).child(wordSnapshot.getKey()).removeValue();
//                                Finals.database.getReference("users").child(Finals.AUTH_INSTANCE.getUid()).child("learned").child(data.getKey()).child(englishWord).setValue(koreanInput);
//                                Finals.myRefChapter.child(Finals.AUTH_INSTANCE.getUid()).child("Words").child("Vocabulary").child(data.getKey()).child(wordSnapshot.getKey()).removeValue();

                                /**
                                 * Wydaje mi sie ze w tym miejscu powinien sprawdzac czy usunal ostatnie sloko z danej listy, jesli tak to przywraca slowka z learnt do vocabulary
                                 */


                            }else if(englishWord.equals(wordSnapshot.getKey())){
                                getView().getTestLearntWordsViewHolder().test_in_korean.setHighlightColor(R.color.incorrect);

                                Toast.makeText(context, "Incorrect", Toast.LENGTH_SHORT).show();

                                MaterialDialog.Builder materialDialog = new MaterialDialog.Builder(context);
                                materialDialog.title("Incorrect answer- Hint");
                                materialDialog.neutralText(wordSnapshot.getValue().toString());
                                materialDialog.onPositive((dialog, which) -> {
                                });
                                materialDialog.show();
                                getView().setFields();
                                getView().getTestLearntWordsViewHolder().test_in_korean.setText("");
                            }
                            //System.out.println(list.get(list.indexOf(englishWord)));
                        }
                    }
                }

                else if (!dataSnapshot.exists()) {
                    Toast.makeText(context, "solved", Toast.LENGTH_SHORT).show();

                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, "solved", Toast.LENGTH_SHORT).show();

            }

        });
        return false;
    }



}
