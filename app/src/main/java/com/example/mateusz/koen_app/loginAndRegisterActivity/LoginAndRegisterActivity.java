package com.example.mateusz.koen_app.loginAndRegisterActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.example.mateusz.koen_app.R;
import com.example.mateusz.koen_app.base.BaseActivity;
import com.example.mateusz.koen_app.finals.Finals;
import com.example.mateusz.koen_app.mainActivity.MainActivity;
import com.example.mateusz.koen_app.sharedPreferences.SaveUserState;

public class LoginAndRegisterActivity extends BaseActivity implements LoginAndRegisterContract.View {

    private LoginAndRegisterContract.Presenter presenter;
    private LoginAndRegisterViewHolder viewHolder;

    private SaveUserState savedUserState = new SaveUserState();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView();
        setPresenter();
        setOnClicks();
    }

    void setView() {
        setContentView(R.layout.activity_login_and_register);
        viewHolder = new LoginAndRegisterViewHolder(findViewById(android.R.id.content));
    }

    private void setOnClicks() {
        viewHolder.login_in_button.setOnClickListener(v -> {
            if (validateFields()) {
                Finals.AUTH_INSTANCE.signInWithEmailAndPassword(viewHolder.email_field.getText().toString(), viewHolder.password_field.getText().toString()).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        if (!Finals.AUTH_INSTANCE.getCurrentUser().isEmailVerified()) {
                            Toast.makeText(this, "Please confirm your email", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(this, "You're logged in!", Toast.LENGTH_SHORT).show();
                            savedUserState.setUserState(this, viewHolder.email_field.getText().toString(), viewHolder.password_field.getText().toString());
                            Intent intent = new Intent(this, MainActivity.class);
                            startActivity(intent);
                            presenter.createUserWordsDatabase();
                        }
                    } else {
                        Toast.makeText(this, "You're not registered", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        viewHolder.register_button.setOnClickListener(task -> {
                    if (validateFields()) {
                        presenter.registrationEmailAndPassword(viewHolder.email_field.getText().toString(), viewHolder.password_field.getText().toString());
                    }
                }
        );
    }

    @Override
    public boolean validateFields() {
        if (TextUtils.isEmpty(viewHolder.email_field.getText().toString())) {
            viewHolder.email_hint.setHint("This field cannot be empty");
            return false;
        }
        if (viewHolder.email_field.getText().toString().length() <= 3) {
            viewHolder.email_hint.setHint("Email must have at least 3 characters");
            return false;
        }
        if (!viewHolder.email_field.getText().toString().contains("@")) {
            viewHolder.email_hint.setHint("Email field must have @");
            return false;
        }
        if (TextUtils.isEmpty(viewHolder.password_field.getText().toString())) {
            viewHolder.password_hint.setHint("This field cannot be empty");
            return false;
        }
        if (viewHolder.password_field.getText().toString().length() <= 3) {
            viewHolder.password_hint.setHint("Password must have at least 3 characters");
            return false;
        }
        return true;
    }


    void setPresenter() {
        presenter = new LoginAndRegisterPresenter();
        ((LoginAndRegisterPresenter) presenter).onLoad(this);
    }

    @Override
    public LoginAndRegisterContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    public LoginAndRegisterViewHolder getLoginAndRegisterViewHolder() {
        return viewHolder;
    }
}
