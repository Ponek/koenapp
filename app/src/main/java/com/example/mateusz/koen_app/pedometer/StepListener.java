package com.example.mateusz.koen_app.pedometer;

// Will listen to step alerts
public interface StepListener {

    public void step(long timeNs);

}


