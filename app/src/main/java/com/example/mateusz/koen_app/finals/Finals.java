package com.example.mateusz.koen_app.finals;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public abstract class Finals {
    public final static FirebaseAuth AUTH_INSTANCE = FirebaseAuth.getInstance();
    public final static FirebaseDatabase database = FirebaseDatabase.getInstance();
    public final static DatabaseReference myRefChapter = database.getReference("users");

}
