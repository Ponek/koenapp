package com.example.mateusz.koen_app.mainActivity;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.mateusz.koen_app.R;
import com.example.mateusz.koen_app.WordDialog;
import com.example.mateusz.koen_app.base.BaseActivity;
import com.example.mateusz.koen_app.editWordActivity.EditWordActivity;
import com.example.mateusz.koen_app.learningActivity.StartLearningActivity;
import com.example.mateusz.koen_app.learntWords.LearntWordsActivity;
import com.example.mateusz.koen_app.pedometer.StepDetector;
import com.example.mateusz.koen_app.pedometer.StepListener;
import com.example.mateusz.koen_app.randomWordNotify.RandomWordNotification;

public class MainActivity extends BaseActivity implements MainContract.View, StepListener, SensorEventListener {

    private MainContract.MainPresenter presenter;
    MainViewHolder viewHolder;


    private StepDetector simpleStepDetector;
    private SensorManager sensorManager;
    private Sensor accel;
    private static final String TEXT_NUM_STEPS = "Number of Steps: ";
    private int numSteps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView();
        setPresenter();

    }

    private void setView() {
        setContentView(R.layout.activity_main);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        simpleStepDetector = new StepDetector();
        simpleStepDetector.registerListener(this);
        numSteps = 0;
        sensorManager.registerListener(MainActivity.this, accel, SensorManager.SENSOR_DELAY_FASTEST);

        viewHolder = new MainViewHolder(findViewById(android.R.id.content));
        viewHolder.start_learning.setOnClickListener(view -> goToStartLearningActivity());
        viewHolder.add_word.setOnClickListener(v -> openDialog());
        viewHolder.edit_word.setOnClickListener(view -> gotoEditWordActivity());
        viewHolder.learnt_words.setOnClickListener(view -> gotToLearntWordsActivity());
    }

    private void gotToLearntWordsActivity() {
        WordDialog wordDialog = new WordDialog();
        wordDialog.createWordsTypeDialog(this, LearntWordsActivity.class);

    }

    @Override
    public void openDialog() {
        WordDialog wordDialog = new WordDialog();
        wordDialog.createEmptyDialog(this);

    }

    @Override
    public void goToStartLearningActivity() {
        WordDialog wordDialog = new WordDialog();
        wordDialog.createWordsTypeDialog(this, StartLearningActivity.class);
    }

    @Override
    public void gotoEditWordActivity() {
        WordDialog wordDialog = new WordDialog();
        wordDialog.createWordsTypeDialog(this, EditWordActivity.class);

    }

    @Override
    public MainContract.MainPresenter getPresenter() {
        return presenter;
    }

    @Override
    public MainViewHolder getMainViewHolder() {
        return viewHolder;
    }

    private void setPresenter() {
        presenter = new MainPresenter();
        presenter.onLoad(this);
    }

    @Override
    public void step(long timeNs) {
        numSteps++;
        if (numSteps == 10) {
            RandomWordNotification test = new RandomWordNotification();
            //na twardo ustawione wordTypes i chapter dlateog tylko z chapteru 1 będzie losowało słówka
            test.getRandomWordAndSendIt(this, "Vocabulary", "Chapter 1");
            numSteps = 0;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        simpleStepDetector.updateAccel(sensorEvent.timestamp, sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2]);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
