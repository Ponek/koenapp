package com.example.mateusz.koen_app.testActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.mateusz.koen_app.R;
import com.example.mateusz.koen_app.Word;
import com.example.mateusz.koen_app.base.BaseActivity;
import com.example.mateusz.koen_app.finals.Finals;
import com.example.mateusz.koen_app.mainActivity.MainActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TestActivity extends BaseActivity implements TestActivityContract.View {

    private TestActivityContract.Presenter presenter;
    private TestActivityViewHolder viewHolder;
    private String wordsType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView();
        Intent intent = getIntent();
        wordsType = intent.getStringExtra("type");
        setPresenter();

        setFields();
        System.out.println(viewHolder.test_in_english.getText().toString());

    }


    void setView() {
        setContentView(R.layout.activity_test);
        viewHolder = new TestActivityViewHolder(findViewById(android.R.id.content));
        viewHolder.test_check.setOnClickListener(v -> presenter.checkWords(this,viewHolder.test_in_korean.getText().toString(), viewHolder.test_in_english.getText().toString(),wordsType));
    }


    @Override
    public void setFields() {
        Finals.database.getReference("users").child(Finals.AUTH_INSTANCE.getUid()).child("set").addListenerForSingleValueEvent(new ValueEventListener() {
            ArrayList<Word> list = new ArrayList<>();

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        Log.d("chapter", data.getKey());
                        for (DataSnapshot wordSnapshot : data.getChildren()) {
                            Log.d("english", wordSnapshot.getKey());
                            Log.d("korean", wordSnapshot.getValue().toString());

                            list.add(new Word(wordSnapshot.getKey(), wordSnapshot.getValue().toString()));
                        }
                    }
                    viewHolder.test_in_english.setText(list.get(presenter.randomNUmber(list.size())).getInEnglish());
                    System.out.println("Slowko wylosowane: " + viewHolder.test_in_english.getText());
                }else{
                    Toast.makeText(TestActivity.this, "Good job!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(TestActivity.this,MainActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }


    @Override
    public TestActivityContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    public TestActivityViewHolder getTestActivityViewHolder() {
        return viewHolder;
    }

    private void setPresenter() {
        presenter = new TestActivityPresenter();
        ((TestActivityPresenter) presenter).onLoad(this);
    }


}
