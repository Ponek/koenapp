package com.example.mateusz.koen_app.editWordActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SearchView;
import android.widget.Spinner;

import com.example.mateusz.koen_app.R;
import com.example.mateusz.koen_app.Word;
import com.example.mateusz.koen_app.base.BaseActivity;
import com.example.mateusz.koen_app.finals.Finals;
import com.example.mateusz.koen_app.listAdapters.EditWordAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class EditWordActivity extends BaseActivity implements EditWordContract.View {

    private EditWordContract.Presenter presenter;
    private EditWordViewHolder viewHolder;

    private int allWords = 0;
    private String wordTypes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView();
        Intent intent = getIntent();
        wordTypes = intent.getStringExtra("type");
        setPresenter();
        setSpinnerChapters();
        setWordsLimits();
    }

    void setView() {
        setContentView(R.layout.activity_edit_word);
        viewHolder = new EditWordViewHolder(findViewById(android.R.id.content));
        viewHolder.search_word.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (!newText.isEmpty()) {
                    Finals.myRefChapter.child(Finals.AUTH_INSTANCE.getUid()).child("Words").child(wordTypes).child(viewHolder.select_chapter.getSelectedItem().toString()).addValueEventListener(new ValueEventListener() {
                        List<Word> list = new ArrayList<>();
                        List<Word> adapterList = new ArrayList<>();
                        List<Integer> randomWordsNumbers;
                        EditWordAdapter editWordAdapter;

                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                for (DataSnapshot iter : dataSnapshot.getChildren()) {
                                    Word word = new Word(iter.getKey(), iter.getValue().toString());
                                    list.add(word);
                                }
                                for (int i = 0; i < list.size(); i++) {
                                    if (list.get(i).getInEnglish().contains(newText)) {
                                        adapterList.add(new Word(list.get(i).getInEnglish(), list.get(i).getInKorean()));

                                    }
                                    editWordAdapter = new EditWordAdapter(adapterList, getActivity(), viewHolder.select_chapter.getSelectedItem().toString(),wordTypes);
                                    editWordAdapter.notifyDataSetChanged();
                                    viewHolder.word_list.setAdapter(editWordAdapter);
                                    viewHolder.word_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }

                return true;
            }
        });
    }

    void setPresenter() {
        presenter = new EditWordActivityPresenter();
        ((EditWordActivityPresenter) presenter).onLoad(this);
    }

    @Override
    public void setWordsList(String chapter, int range) {
        Finals.myRefChapter.child(Finals.AUTH_INSTANCE.getUid()).child("Words").child(wordTypes).child(chapter).addListenerForSingleValueEvent(new ValueEventListener() {
            List<Word> list = new ArrayList<>();
            List<Word> adapterList = new ArrayList<>();
            EditWordAdapter editWordAdapter;

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot iter : dataSnapshot.getChildren()) {
                        Word word = new Word(iter.getKey(), iter.getValue().toString());
                        list.add(word);
                        allWords++;
                    }


                    if(list.size() < range) {
                        for (int i = 0; i < list.size(); i++) {
                            adapterList.add(new Word(list.get(i).getInEnglish(), list.get(i).getInKorean()));
                        }
                    }else {
                        for (int i = 0; i < range; i++) {
                            adapterList.add(new Word(list.get(i).getInEnglish(), list.get(i).getInKorean()));
                        }
                    }



                    editWordAdapter = new EditWordAdapter(adapterList, getActivity(), chapter,wordTypes);
                    editWordAdapter.notifyDataSetChanged();
                    viewHolder.word_list.setAdapter(editWordAdapter);
                    viewHolder.word_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void searchWord() {

    }

    @Override
    public void setWordsLimits() {

        viewHolder.select_range.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                tempRef = Finals.myRefAddWordsSet.push();

                String rozmiar = viewHolder.select_range.getSelectedItem().toString().replace(" words", " ").trim();
                if (rozmiar.equals("All")) {
                    setWordsList(viewHolder.select_chapter.getSelectedItem().toString(), allWords);
                    System.out.println("Rozmiar all: " + "chuj");
                } else {
                    setWordsList(viewHolder.select_chapter.getSelectedItem().toString(), Integer.valueOf(viewHolder.select_range.getSelectedItem().toString().replace(" words", " ").trim()));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
        viewHolder.select_chapter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                tempRef = Finals.myRefAddWordsSet.push();

                String rozmiar = viewHolder.select_range.getSelectedItem().toString().replace(" words", " ").trim();
                if (rozmiar.equals("All")) {
                    setWordsList(viewHolder.select_chapter.getSelectedItem().toString(), allWords);
                    System.out.println("Rozmiar all: " + "pizda");
                } else {
                    setWordsList(viewHolder.select_chapter.getSelectedItem().toString(), Integer.valueOf(viewHolder.select_range.getSelectedItem().toString().replace(" words", " ").trim()));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void setSpinnerChapters() {
        Finals.myRefChapter.child(Finals.AUTH_INSTANCE.getUid()).child("Words").child(wordTypes).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> chaptersList = new ArrayList<>();
                if (dataSnapshot.exists()) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        Log.d("chapters", data.getKey());
                        chaptersList.add(data.getKey());
                    }


                    ArrayAdapter<String> chaptersAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, chaptersList);
                    viewHolder.select_chapter.setAdapter(chaptersAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public EditWordViewHolder getEditWordViewHolder() {
        return viewHolder;
    }

    @Override
    public EditWordContract.Presenter getPresenter() {
        return presenter;
    }

}
