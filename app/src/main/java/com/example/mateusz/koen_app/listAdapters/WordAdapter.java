package com.example.mateusz.koen_app.listAdapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mateusz.koen_app.R;
import com.example.mateusz.koen_app.Word;

import java.util.List;

public class WordAdapter extends RecyclerView.Adapter<WordAdapter.MyViewHolder> {
    private List<Word> list;
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView word_in_english, word_int_korean;

        public MyViewHolder(View v) {
            super(v);
            word_in_english = (TextView) itemView.findViewById(R.id.word_in_english);
            word_int_korean = (TextView) itemView.findViewById(R.id.word_in_korean);
        }
    }

    public WordAdapter(List<Word> list) {
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.words_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Word word = list.get(position);
        holder.word_in_english.setText(word.getInEnglish());
        holder.word_int_korean.setText(word.getInKorean());


    }

    @Override
    public int getItemCount() {
        return list .size();
    }
}
