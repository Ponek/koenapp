package com.example.mateusz.koen_app.randomWordNotify;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.example.mateusz.koen_app.R;
import com.example.mateusz.koen_app.Word;
import com.example.mateusz.koen_app.finals.Finals;
import com.example.mateusz.koen_app.listAdapters.WordAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.nio.channels.Channel;
import java.util.ArrayList;
import java.util.List;

public class RandomWordNotification extends Thread {

    public void getRandomWordAndSendIt(Context context, String wordTypes, String chapter) {
        Finals.database.getReference("users").child(Finals.AUTH_INSTANCE.getCurrentUser().getUid()).child("set").removeValue();
        Finals.myRefChapter.child(Finals.AUTH_INSTANCE.getUid()).child("Words").child(wordTypes).child(chapter).addListenerForSingleValueEvent(new ValueEventListener() {
            List<Word> list = new ArrayList<>();
            Word wordToSend;

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot iter : dataSnapshot.getChildren()) {
                        Word word = new Word(iter.getKey(), iter.getValue().toString());
                        list.add(word);
                    }
                    wordToSend = list.get((int) (Math.random() * ((list.size() - 1) + 1)));


                    NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "112")
                            .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                            .setContentTitle("Learn new word")
                            .setContentText(wordToSend.toString())
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT);

                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

                    notificationManager.notify(112, builder.build());
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
