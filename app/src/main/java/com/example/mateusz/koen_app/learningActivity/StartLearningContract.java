package com.example.mateusz.koen_app.learningActivity;

import android.widget.ArrayAdapter;

import java.util.List;

interface StartLearningContract {
    interface View{
        StartLearningViewHolder getStartLearningViewHolder();
        StartLearningContract.Presenter getPresetner();
        void setWordsList(String chapter,int range);
        void setWordsLimits();
        void onStartTestClick();
        void setSpinnerChapters();
    }
    interface Presenter{
        List<Integer> randomWordsNumber(int size);

    }
}
