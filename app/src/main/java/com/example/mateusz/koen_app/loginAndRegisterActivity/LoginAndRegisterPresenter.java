package com.example.mateusz.koen_app.loginAndRegisterActivity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.mateusz.koen_app.base.BasePresenter;
import com.example.mateusz.koen_app.finals.Finals;
import com.example.mateusz.koen_app.sharedPreferences.SaveUserState;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

public class LoginAndRegisterPresenter extends BasePresenter<LoginAndRegisterContract.View> implements LoginAndRegisterContract.Presenter {

    private SaveUserState savedUserState = new SaveUserState();

    @Override
    public boolean registrationEmailAndPassword(String email, String password) {
        Finals.AUTH_INSTANCE.createUserWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                sendEmailVerification();
                Toast.makeText((Context) getView(), "Email sended \n Please confirm you email", Toast.LENGTH_SHORT).show();
                Log.d("Sign in", "success");
            } else {
                Toast.makeText((Context) getView(), "You're already registered", Toast.LENGTH_SHORT).show();

                Log.d("Sign in", "failed");
            }

        });
        return false;
    }

    @Override
    public boolean loginEmailAndPassword(String email, String password) {
        Finals.AUTH_INSTANCE.signInWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
//                if (!Finals.AUTH_INSTANCE.getCurrentUser().isEmailVerified()) {
//                    Toast.makeText((Context) getView(), "Please confirm your email", Toast.LENGTH_SHORT).show();
//                } else {
                    Toast.makeText((Context) getView(), "You're logged in!", Toast.LENGTH_SHORT).show();
                    savedUserState.setUserState(((Context) getView()).getApplicationContext(), email, password);
//                }
            }
            else{
                Toast.makeText((Context) getView(), "You're not registered", Toast.LENGTH_SHORT).show();
            }
        });
        return false;
    }

    @Override
    public void sendEmailVerification() {
        Finals.AUTH_INSTANCE.getCurrentUser().sendEmailVerification().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Log.d("Email verification", "Email sended");
            } else {
                Log.d("Email verification", "Email not sended");
            }
        });
    }

    @Override
    public void createUserWordsDatabase() {
        Finals.database.getReference().child("Words").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    Log.d("words database","DUPLICATION");
                    Finals.database.getReference("users").child(Finals.AUTH_INSTANCE.getUid()).child(dataSnapshot.getKey()).setValue(dataSnapshot.getValue());

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
