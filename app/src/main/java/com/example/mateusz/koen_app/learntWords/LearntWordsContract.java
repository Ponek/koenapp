package com.example.mateusz.koen_app.learntWords;

import java.util.List;

interface LearntWordsContract {

    interface View{
        LearntWordsViewHolder getLearntWordsViewHolder();
        LearntWordsContract.Presenter getPresenter();
        void setSpinnerChapters();
        void setWordsLimit();
        void setWordsList(String chapter,int range);
        void onStartTestClick();

    }
    interface Presenter{
        List<Integer> randomWordsNumber(int size);
    }
}
