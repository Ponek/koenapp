package com.example.mateusz.koen_app.learntWords;

import com.example.mateusz.koen_app.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LearntWordsPresenter extends BasePresenter<LearntWordsContract.View>  implements LearntWordsContract.Presenter{


    @Override
    public List<Integer> randomWordsNumber(int size) {
        List<Integer> list = new ArrayList<Integer>();
        Random rand = new Random();

        do {
            int randomInt = rand.nextInt(size + 1);

            if(!list.contains(randomInt)){
                list.add(randomInt);
            }

        } while (list.size() != size);

        return list;

    }

}
