package com.example.mateusz.koen_app.listAdapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mateusz.koen_app.R;
import com.example.mateusz.koen_app.Word;
import com.example.mateusz.koen_app.WordDialog;

import java.util.List;

public class EditWordAdapter extends RecyclerView.Adapter<EditWordAdapter.MyViewHolder> {
    private List<Word> list;
    private Activity activity;
    private WordDialog wordDialog = new WordDialog();
    private String wordsType;

    private String oldChapter;
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView word_in_english, word_int_korean;

        public MyViewHolder(View v) {
            super(v);
            word_in_english = (TextView) itemView.findViewById(R.id.word_in_english);
            word_int_korean = (TextView) itemView.findViewById(R.id.word_in_korean);
        }

    }

    public EditWordAdapter(List<Word> list, Activity activity,String oldChapter,String wordsType) {
        this.list = list;
        this.activity = activity;
        this.oldChapter = oldChapter;
        this.wordsType = wordsType;
    }

    @Override
    public EditWordAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.words_list, parent, false);

        return new EditWordAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(EditWordAdapter.MyViewHolder holder, int position) {
        Word word = list.get(position);
        holder.word_in_english.setText(word.getInEnglish());
        holder.word_int_korean.setText(word.getInKorean());
        holder.itemView.setOnClickListener(v -> {
            Log.d("here", word.toString());
            wordDialog.createDialogToEditWord(activity, word.getInEnglish(), word.getInKorean(),oldChapter,wordsType);
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
