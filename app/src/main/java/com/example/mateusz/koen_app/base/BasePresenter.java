package com.example.mateusz.koen_app.base;

public abstract class BasePresenter<T>{
    private T view = null;

    public void onLoad(T view){
        this.view = view;
    }
    protected T getView(){
        if(view == null){
            throw new IllegalStateException("Initialize presenter first");
        }
        return view;
    }
}
