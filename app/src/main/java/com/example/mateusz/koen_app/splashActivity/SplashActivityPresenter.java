package com.example.mateusz.koen_app.splashActivity;

import android.content.Context;

import com.example.mateusz.koen_app.base.BasePresenter;
import com.example.mateusz.koen_app.finals.Finals;
import com.example.mateusz.koen_app.sharedPreferences.SaveUserState;

public class SplashActivityPresenter extends BasePresenter<SplashActivityContract.View> implements SplashActivityContract.Presenter {
    SaveUserState saveUserState= new SaveUserState();

    @Override
    public boolean checkUserState() {
        if (!saveUserState.getUserPassword((Context) getView()).isEmpty() && !saveUserState.getUserEmail((Context) getView()).isEmpty()) {
           Finals.AUTH_INSTANCE.signInWithEmailAndPassword(saveUserState.getUserEmail((Context) getView()),saveUserState.getUserPassword((Context) getView()));
            return Finals.AUTH_INSTANCE.getCurrentUser().isEmailVerified();

        } else {
            return false;
        }
    }
}
